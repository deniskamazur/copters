package main

import (
	"flag"
	"fmt"

	"golang.org/x/crypto/ssh"
)

var (
	delay = flag.Int("telemetry-delay", 1, "time after which every telemetry is called")

	user     = flag.String("ssh-user", "deniska", "ssh user")
	password = flag.String("ssh-password", "kerneltrick!9", "ssh password")

	machines = []string{"192.168.1.185", "192.168.1.185"} // "192.168.1.121", "192.168.1.122", "192.168.1.123"}

	sshConfig = &ssh.ClientConfig{
		User: *user,
		Auth: []ssh.AuthMethod{
			ssh.Password(*password),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}
)

func connectSSH(addr string, config *ssh.ClientConfig) (sess *ssh.Session) {
	conn, err := ssh.Dial("tcp", addr, config)
	if err != nil {
		panic(err)
	}

	sess, err = conn.NewSession()
	if err != nil {
		panic(err)
	}

	return sess
}

func main() {
	sess := connectSSH(machines[0]+":22", sshConfig)

	b, err := sess.CombinedOutput("ls")
	fmt.Println(string(b), err)
	fmt.Println(string(b), err)
}

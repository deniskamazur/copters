package main

import (
	"bytes"
	"flag"
	"fmt"
	"html/template"
	"net/http"
	"path/filepath"
	"strings"
	"time"

	"golang.org/x/crypto/ssh"
)

type copterStates map[string]string

var (
	delay = flag.Int("telemetry-delay", 1, "time after which every telemetry is called")

	user     = flag.String("ssh-user", "deniska", "ssh user")
	password = flag.String("ssh-password", "kerneltrick!9", "ssh password")

	machines = []string{"192.168.1.185", "192.168.1.185"} // "192.168.1.121", "192.168.1.122", "192.168.1.123"}

	sshConfig = &ssh.ClientConfig{
		User: *user,
		Auth: []ssh.AuthMethod{
			ssh.Password(*password),
		},
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
	}

	states copterStates

	files, _     = filepath.Glob("templates/*")
	templates, _ = template.New("").ParseFiles(files...)
)

func getTelemetry(addr string, config *ssh.ClientConfig) (b bytes.Buffer) {
	conn, err := ssh.Dial("tcp", addr, config)
	if err != nil {
		return b
	}
	defer conn.Close()

	sess, _ := conn.NewSession()
	if err != nil {
		return b
	}
	defer sess.Close()

	sess.Stdout = &b
	sess.Run("date")

	return b
}

func main() {
	states = make(copterStates)
	for _, machine := range machines {
		states[machine] = ""
	}

	go func() {
		for {
			time.Sleep(time.Duration(*delay))

			for machine := range states {
				bytes := getTelemetry(machine+":22", sshConfig)
				telemetry := string(bytes.Bytes())

				states[machine] = telemetry

			}
		}
	}()

	http.HandleFunc("/copter/", copterStateHandler)
	http.HandleFunc("/explore/", exploreIPsHandler)
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		templates.ExecuteTemplate(w, "main.html", struct{}{})
	})
	http.Handle("/static/", http.StripPrefix("/static", http.FileServer(http.Dir("./static"))))

	http.ListenAndServe(":8000", nil)
}

func copterStateHandler(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w, r)

	pieces := strings.Split(r.URL.Path, "/")
	ip := pieces[len(pieces)-1]

	if state, ok := states[ip]; ok {
		fmt.Fprintf(w, "%s", state)
	} else {
		http.Error(w, fmt.Sprintf("ip %s not found", ip), http.StatusInternalServerError)
	}
}

func exploreIPsHandler(w http.ResponseWriter, r *http.Request) {
	setupResponse(&w, r)
	fmt.Fprintf(w, fmt.Sprintf("[\"%s\"]", strings.Join(machines, `","`)))
}

func setupResponse(w *http.ResponseWriter, req *http.Request) {
	(*w).Header().Set("Access-Control-Allow-Origin", "*")
	(*w).Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	(*w).Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
}

window.onload = function () {
    var machines = document.getElementById("copters")

    $.getJSON("http://localhost:8000/explore/", function(ips, status){
        ips.forEach(ip => {
            var copter = document.createElement("div");
            copter.classList.add("copter");
            copter.classList.add("col-sm")

            var name = document.createElement("p")
            name.innerText = ip
            copter.appendChild(name)

            var cstatus = document.createElement("p")
            copter.appendChild(cstatus)

            machines.appendChild(copter)

            window.setInterval(function() {
                $.get("http://localhost:8000/copter/" + ip, function(data, status) {
                    cstatus.innerText = data
                    console.log(data)
                })
            }, 1000)
        });
    });
}

/*
window.setInterval(function(copter){
            $.get("http://localhost:8000/copter/" + ips[0], function(data, status) {
                machines.innerText = data
            }
        )}, 1000)*/